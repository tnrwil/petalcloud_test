import requests,json
from flask import Flask,request, jsonify,make_response,url_for,redirect
from json import dumps
from requests import post


url = 'http://api.shoutcloud.io/V1/SHOUT'

app = Flask(__name__)
users_seen = {}


@app.route('/')
def hello():
    return 'hello world' 


#return reverse data
#@app.route('/reverse/<data>', methods=['POST'])
@app.route('/reverse', methods=['POST'])
def reverse_data():
    input = request.get_json(force=True)
    data=input['data']
    rev_data=data[::-1]
    response = requests.post(url = 'http://api.shoutcloud.io/V1/SHOUT',data = json.dumps({"INPUT": rev_data}), headers={'Content-Type': 'application/json'})
    output=json.loads(response.text)
    return jsonify(data=output["OUTPUT"])
    #return output["OUTPUT"]
    #return jsonify(data=output["OUTPUT"])
    #return response.text



def checkingin():
           return "Checking in!"


@app.route("/hello/<name>")
def hello_name(name):
        return "Hello, {}".format(name)



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
